import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  private headers;
  private authToken: any;
  private user: any;

  constructor(private http: Http) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
  }

  registerUser(user) {
    return this.http.post('users/register', user, {headers: this.headers})
      .map(response => response.json());
  }

  authenticateUser(user) {
    return this.http.post('users/authenticate', user, {headers: this.headers})
      .map(response => response.json());
  }

  storeUserData(token, user) {
    localStorage.setItem('token_name', token);
    localStorage.setItem('user', JSON.stringify(user));

    this.authToken = token;
    this.user = user;
  }

  getProfile() {
    let headers = new Headers();

    this.loadToken();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authToken);

    return this.http.get('users/profile', {headers: headers})
      .map(response => response.json());
  }

  loadToken() {
    this.authToken = localStorage.getItem('token_name');
  }

  loggedIn() {
    return tokenNotExpired('token_name');
  }

  logout() {
    this.authToken = null;
    this.user = null;

    localStorage.clear();
  }
}
