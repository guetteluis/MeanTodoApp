import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable()
export class SidebarService {

  private sidebarOptionsSource = new Subject<any>();

  toggle$ = this.sidebarOptionsSource.asObservable();

  constructor() { }

  toggle(){
    this.sidebarOptionsSource.next();
  }
}
