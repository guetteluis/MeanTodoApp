import { Injectable } from '@angular/core';

@Injectable()
export class ValidatorService {

  constructor() { }

  validateRegistrationFields(user){
    if (user.email == '' || user.username == '' || user.first_name == '' || user.last_name == '' ||
      user.password == '' || user.repeat_password == ''){
      return {
        success: false,
        message: "Please, fill in all the fields"
      }
    }

    if (!this.validateEmail(user.email)){
      return {
        success: false,
        message: "Enter a valid email"
      };
    }

    if (!this.validatePassword(user.password, user.repeat_password)){
      return {
        success: false,
        message: "Passwords do not match"
      };
    }

    return {
      success: true,
      message: ""
    };
  }

  validateEmail(email:string){
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validatePassword(password:string, repeat_password: string) {
    return password == repeat_password;
  }

  validateLoginFields(user) {
    if (user.username == '' || user.password == '') {
      return {
        success: false,
        message: "Please, fill in all the fields"
      };
    }

    return {
      success: true,
    };
  }

}
