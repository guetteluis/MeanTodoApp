import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable()
export class AlertsService {
  private alertOptionsSource = new Subject<any>();

  showAlert$ = this.alertOptionsSource.asObservable();

  constructor() { }

  show(options: {cssClass: string, message: string, timeout: number}){
    this.alertOptionsSource.next(options);
  }

}
