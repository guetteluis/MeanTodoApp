import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { SidebarModule } from 'ng-sidebar';
import { DndModule } from 'ng2-dnd';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AlertsComponent } from './components/alerts/alerts.component';

import { ValidatorService } from './services/validator.service';
import { AlertsService } from './services/alerts.service';
import { AuthService } from './services/auth.service';

import { AuthGuard } from './guards/auth-guard';
import { SidebarService } from "./services/sidebar.service";


const appRoutes: Routes = [
  {path:'', component: HomeComponent},
  {path:'register', component: RegisterComponent},
  {path:'login', component: LoginComponent},
  {path:'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path:'profile', component: ProfileComponent, canActivate: [AuthGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    AlertsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    SidebarModule.forRoot(),
    DndModule.forRoot()
  ],
  providers: [
    ValidatorService,
    AlertsService,
    AuthService,
    AuthGuard,
    SidebarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
