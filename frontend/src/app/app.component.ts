import { Component } from '@angular/core';
import { Subscription } from "rxjs";
import { SidebarService } from "./services/sidebar.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  _opened: boolean = false;
  _sidebarClass: string = 'bg-primary';
  _mode: string = 'push';
  _position: string = 'left';
  _closeOnClickOutside: boolean = true;
  _keyClose: boolean = true;

  subscription: Subscription;

  constructor(private sidebar: SidebarService) {
    this.subscription = sidebar.toggle$.subscribe(() => {
      this.toggleSidebar()
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  private toggleSidebar() {
    this._opened = !this._opened;
  }
}
