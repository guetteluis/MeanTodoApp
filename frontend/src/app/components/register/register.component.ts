import {Component, OnInit} from '@angular/core';

import { ValidatorService } from '../../services/validator.service';
import { AlertsService } from '../../services/alerts.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  email: string = '';
  username: string = '';
  password: string = '';
  repeat_password: string = '';
  first_name: string = '';
  last_name: string = '';
  validation: {
    success: boolean,
    message: string
  };

  constructor(
    private validator: ValidatorService,
    private alertService: AlertsService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onRegisterSubmit(){
    const user = {
      email: this.email,
      username: this.username,
      password: this.password,
      repeat_password: this.repeat_password,
      first_name: this.first_name,
      last_name: this.last_name
    };

    this.validation = this.validator.validateRegistrationFields(user);

    if (!this.validation.success){
      this.alertService.show({
        cssClass: 'alert-danger',
        message: this.validation.message,
        timeout: 3000
      });
    } else {
      this.authService.registerUser(user).subscribe((data) => {
        if (! data.success) {
          this.alertService.show({
            cssClass: 'alert-danger',
            message: data.message,
            timeout: 3000
          });
          this.router.navigate(['/register']);
        } else {
          this.alertService.show({
            cssClass: 'alert-success',
            message: data.message,
            timeout: 3000
          });
          this.router.navigate(['/login']);
        }
      });
    }
  }
}
