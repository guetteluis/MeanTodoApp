import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { AlertsService } from '../../services/alerts.service';
import { ValidatorService } from '../../services/validator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: String = '';
  password: String = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private alert: AlertsService,
    private validator: ValidatorService
  ) { }

  ngOnInit() {
  }

  onLoginSubmit() {
    let validation;
    const user = {
      username: this.username,
      password: this.password
    };

    validation = this.validator.validateLoginFields(user);

    if (!validation.success){

      this.showMessage('alert-danger', validation.message);
      this.router.navigate(['/login']);

    } else {

      this.authService.authenticateUser(user).subscribe(data => {
        if (!data.success){

          this.showMessage('alert-danger', data.message);
          this.router.navigate(['/login']);

        } else {

          this.authService.storeUserData(data.token, data.user);
          this.showMessage('alert-success', data.message);
          this.router.navigate(['/dashboard']);

        }
      });

    }
  }

  showMessage(cssClass: string, message: string) {
    this.alert.show({
      cssClass: cssClass,
      message: message,
      timeout: 3000
    });
  }
}
