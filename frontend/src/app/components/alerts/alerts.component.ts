import {Component, OnInit, OnDestroy} from '@angular/core';

import { AlertsService } from '../../services/alerts.service';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit, OnDestroy{

  options: {
    cssClass: string,
    message: string
  };
  visibility = false;

  subscription: Subscription;

  constructor(private alertsService: AlertsService) {
    this.subscription = alertsService.showAlert$.subscribe(
      options => {
        this.options = options;
        this.visibility = true;
        setTimeout(()=>{ this.visibility = false }, options.timeout);
      }
    );
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
  }
}
