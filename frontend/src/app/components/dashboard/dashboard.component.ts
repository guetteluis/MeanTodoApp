import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dragOperation: boolean = true;

  containers: Array<Container> = [
    new Container(1, 'Container 1', [new Widget('1'), new Widget('2')]),
    new Container(2, 'Container 2', [new Widget('3'), new Widget('4')]),
    new Container(3, 'Container 3', [new Widget('5'), new Widget('6')])
  ];

  constructor() { }

  ngOnInit() {
  }

}

class Container {
  constructor(public id: number, public name: string, public widgets: Array<Widget>) {}
}

class Widget {
  constructor(public name: string) {}
}
