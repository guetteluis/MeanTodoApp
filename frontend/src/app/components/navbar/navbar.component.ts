import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { AlertsService } from '../../services/alerts.service';
import { SidebarService } from '../../services/sidebar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    private alert: AlertsService,
    private sidebar: SidebarService
  ) { }

  ngOnInit() {
  }

  onLogoutClick() {
    this.authService.logout();

    this.alert.show({
      cssClass: 'alert-success',
      message: 'Hope to see you again',
      timeout: 3000
    });

    this.router.navigate(['/login']);

    return false;
  }

  isLoggedIn() {
    return this.authService.loggedIn();
  }

  toggleSidebar() {
    this.sidebar.toggle();
  }

}
