import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { AlertsService } from '../services/alerts.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private auth: AuthService,
    private router: Router,
    private alert: AlertsService
  ) {}

  canActivate() {
    if(this.auth.loggedIn()) {
      return true;
    } else {
      this.router.navigate(['']);

      this.alert.show({
        cssClass: 'alert-danger',
        message: 'Wrong Route',
        timeout: 3000
      });

      return false;
    }
  }
}
