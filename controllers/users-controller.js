const User = require('../models/user');
const jwt = require('jsonwebtoken');
const jwtConfig = require('../config/jwt');

exports.register = function (request, response, next) {
    let newUser = new User({
        email: request.body.email,
        username: request.body.username,
        password: request.body.password,
        first_name: request.body.first_name,
        last_name: request.body.last_name
    });

    User.create(newUser, (error, user) => {
        if (error) {
            if (error.errors.username){
                response.json({
                    success: false,
                    message: 'Username already in use'
                });
            } else if (error.errors.email) {
                response.json({
                    success: false,
                    message: 'Email already in use'
                });
            }
        } else {
            response.json({
                success: true,
                message: 'Now you are part of us!',
                user: user
            });
        }
    });
};

exports.auth = function (request, response, next) {
    const username = request.body.username;
    const password = request.body.password;

    User.getByUsername(username, (error, user) => {
        if (error) throw error;

        if (!user) {
            return response.json({
                success: false,
                message: 'User not found'
            });
        }

        User.comparePassword(password, user.password, (error, isMatch) => {
            if (error) throw error;

            if (isMatch){
                const token = jwt.sign(user, jwtConfig.secret, {
                    expiresIn: jwtConfig.expireTime
                });

                return response.json({
                    success: true,
                    token: 'JWT ' + token,
                    message: 'Welcome back!',
                    user: {
                        id: user._id,
                        email: user.email,
                        username: user.username,
                        first_name: user.first_name,
                        last_name: user.last_name
                    }
                });
            } else {
                return response.json({
                    success: false,
                    message: 'Wrong password'
                });
            }
        });
    });
};

exports.getProfile = function (request, response, next) {
    let profile = {
        first_name: request.user.first_name,
        last_name: request.user.last_name,
        username: request.user.username,
        email: request.user.email
    };
    response.send(profile);
};