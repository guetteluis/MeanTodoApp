const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
   email: {
       type: String,
       required: true,
       unique: true
   },
    username: {
       type: String,
        required: true,
        unique: true
    },
    password: {
       type: String,
        required: true
    },
    first_name: {
       type: String
    },
    last_name: {
        type: String
    }
});

UserSchema.plugin(uniqueValidator);

const User = mongoose.model('User', UserSchema);

module.exports = User;

module.exports.getById = function (id, callback) {
    User.findById(id, callback);
};

module.exports.getByUsername = function (username, callback) {
    const query = {username: username};
    User.findOne(query, callback);
};

/*module.exports.create = function (newUser, callback) {
    genPasswordHash(newUser, (error, hash) => {
        if (error) {
            callback(error, null);
        } else {
            newUser.password = hash;
            newUser.save((error) => {
                if (error){
                    callback(error, null);
                }
            });
        }
    });
};*/

module.exports.create = function (newUser, callback) {
    genPasswordHash(newUser, (error, hash) => {
        if (error) {
            callback(error, null);
        } else {
            newUser.password = hash;
            newUser.save((error) => {
                callback(error);
            })
        }
    });
};

function genPasswordHash(newUser, callback) {
    bcrypt.genSalt(10, (error, salt) => {
        bcrypt.hash(newUser.password, salt, callback);
    });
}

module.exports.comparePassword = function (candidatePassword, hash, callback){
    bcrypt.compare(candidatePassword, hash, (error, isMatch) => {
        if (error) throw error;
        callback(null, isMatch);
    });
};
