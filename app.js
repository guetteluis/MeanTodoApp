const env = require('node-env-file');
const fileExists = require('file-exists');
const express = require('express');
const mongoose = require('mongoose');
const database = require('./config/database');
const cors = require('cors');
const path = require('path');
const bodyParser = require('body-parser');
const passport = require('passport');
const strategy = require('./config/passport');

const envFileLocation = __dirname + '/.env';
const userRoutes = require('./routes/users');

// .env file configuration
if (fileExists.sync(envFileLocation)){
    env(envFileLocation);
}

// Express initialize
const app = express();

// MongoDB connection
mongoose.connect(database.mongodb);
mongoose.Promise = global.Promise;

// On successful connection
mongoose.connection.on('connected', () => {
   console.log('connected to database');
});

// On connection error
mongoose.connection.on('error', (error) => {
   console.log('database error: ' + error);
});

// CORS middleware
app.use(cors());

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser middleware
app.use(bodyParser.json());

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

strategy(passport);

// Routes
app.use('/users', userRoutes);

// Index Route
app.get('/', (request, response) => {
   response.send('Invalid Endpoint');
});

app.get('*', (request, response) => {
    response.sendFile(path.join(__dirname, 'public/index.html'));
});

// Start server
const server = app.listen(process.env.PORT || 3000, '0.0.0.0', () => {
    const port = server.address().port;
    console.log('app running on port', port);
});