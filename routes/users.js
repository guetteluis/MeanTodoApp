const express = require('express');
const router = express.Router();
const passport = require('passport');

const UsersController = require('../controllers/users-controller');

router.post('/register', UsersController.register);

router.post('/authenticate', UsersController.auth);

router.get('/profile', passport.authenticate('jwt', {session: false}), UsersController.getProfile);

module.exports = router;