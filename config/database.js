const env = require('node-env-file');
const fileExists = require('file-exists');

const envFileLocation = process.cwd() + '/.env';

// .env file configuration
if (fileExists.sync(envFileLocation)){
    env(envFileLocation);
}

module.exports = {
    mongodb: 'mongodb://' +
    process.env.MONGO_DB_USERNAME + ':' +
    process.env.MONGO_DB_PASSWORD + '@' +
    process.env.MONGO_DB_HOST + ':' +
    process.env.MONGO_DB_PORT + '/' +
    process.env.MONGO_DB_DATABASE +
    process.env.MONGO_DB_PARAMETERS
};