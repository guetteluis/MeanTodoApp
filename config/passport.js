const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');
const jwtConfig = require('./jwt');

module.exports = function (passport) {
    let options = {};
    options.jwtFromRequest = ExtractJwt.fromAuthHeader();
    options.secretOrKey = jwtConfig.secret;

    passport.use(new JwtStrategy(options, (jwt_payload, done) => {
        User.getById(jwt_payload._doc, (error, user) => {
            if (error){
                return done(error, false);
            }

            if (user){
                return done(null, user);
            } else {
                return done(null, false);
            }
        });
    }));
};